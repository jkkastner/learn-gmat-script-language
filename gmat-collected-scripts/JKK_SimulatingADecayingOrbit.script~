% -*- mode: gmat; coding: utf-8 -*-
%Time-stamp: "2021-07-23 00:21:04 vagrant"

%General Mission Analysis Tool(GMAT) Script
%Original:
%  /home/vagrant/gmat/application/samples/Tut_SimulatingAnOrbit.script
%  -or-
%  /home/vagrant/gmat/GMAT-R2020a-Linux-x64/samples/Tut_SimulatingAnOrbit.script
%Created: 2011-11-23 10:08:38
%Tutorial:
%  http://gmat.sourceforge.net/docs/R2020a/html/SimulatingAnOrbin.html

%Saved to: /home/vagrant/gmat/GMAT-R2020a-Linux-x64/bin/

%Preparation:
% # Edit /home/vagrant/gmat/application/bin/gmat_startup_file.txt
%     Comment out MATLAB related lines (comment lines start with #):
%     #PLUGIN   = ../plugins/libMatlabInterface
%     #PLUGIN   = ../plugins/libFminconOptimizer
%     ...
%     #MATLAB_FUNCTIOL_PATH   = ROOT_PATH/matlab
%     #MATLAB_FUNCTIOL_PATH   = ROOT_PATH/userfunctions/matlab
%
% # Note: In the future, we may be able to substitute GNU's open source
%         replacement, Octave, for proprietary product MATLAB.
%
% # Copy atmosphere drag force model into expected directory
% cd /home/vagrant/gmat/application
% sudo cp samples/SupportFiles/CSSI_2004To2026.txt data/atmosphere/earth/

%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft DigitalSat;
GMAT DigitalSat.DateFormat = UTCGregorian;
GMAT DigitalSat.Epoch = '22 Jul 2014 11:29:10.811';
GMAT DigitalSat.CoordinateSystem = EarthMJ2000Eq;
GMAT DigitalSat.DisplayStateType = Keplerian;
GMAT DigitalSat.SMA = 83474.31800000001;
GMAT DigitalSat.ECC = 0.89652;
GMAT DigitalSat.INC = 12.4606;
GMAT DigitalSat.RAAN = 292.8362;
GMAT DigitalSat.AOP = 218.9805;
GMAT DigitalSat.TA = 180;
GMAT DigitalSat.DryMass = 850;
GMAT DigitalSat.Cd = 2.2;
GMAT DigitalSat.Cr = 1.8;
GMAT DigitalSat.DragArea = 15;
GMAT DigitalSat.SRPArea = 1;
GMAT DigitalSat.NAIFId = -123456789;
GMAT DigitalSat.NAIFIdReferenceFrame = -123456789;
GMAT DigitalSat.Id = 'DigitalSatId';
GMAT DigitalSat.Attitude = CoordinateSystemFixed;
GMAT DigitalSat.ModelFile = '../data/vehicle/models/aura.3ds';
GMAT DigitalSat.ModelOffsetX = 0;
GMAT DigitalSat.ModelOffsetY = 0;
GMAT DigitalSat.ModelOffsetZ = 0;
GMAT DigitalSat.ModelRotationX = 0;
GMAT DigitalSat.ModelRotationY = 0;
GMAT DigitalSat.ModelRotationZ = 0;
GMAT DigitalSat.ModelScale = 1;
GMAT DigitalSat.AttitudeDisplayStateType = 'Quaternion';
GMAT DigitalSat.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT DigitalSat.AttitudeCoordinateSystem = 'EarthMJ2000Eq';

Create ForceModel LowEarthProp_ForceModel;
GMAT LowEarthProp_ForceModel.CentralBody = Earth;
GMAT LowEarthProp_ForceModel.PrimaryBodies = {Earth};
GMAT LowEarthProp_ForceModel.PointMasses = {Luna, Sun};
GMAT LowEarthProp_ForceModel.SRP = On;
GMAT LowEarthProp_ForceModel.RelativisticCorrection = Off;
GMAT LowEarthProp_ForceModel.ErrorControl = RSSStep;
GMAT LowEarthProp_ForceModel.GravityField.Earth.Degree = 10;
GMAT LowEarthProp_ForceModel.GravityField.Earth.Order = 10;
GMAT LowEarthProp_ForceModel.GravityField.Earth.PotentialFile = 'JGM2.cof';
GMAT LowEarthProp_ForceModel.GravityField.Earth.TideModel = 'None';
%GMAT LowEarthProp_ForceModel.Drag.AtmosphereModel = 'JacchiaRoberts';
%GMAT LowEarthProp_ForceModel.Drag.F107 = 150;
%GMAT LowEarthProp_ForceModel.Drag.F107A = 150;
%GMAT LowEarthProp_ForceModel.Drag.MagneticIndex = 3;
%GMAT LowEarthProp_ForceModel.Drag.F107 = 150;
%GMAT LowEarthProp_ForceModel.Drag.F107A = 150;
%GMAT LowEarthProp_ForceModel.Drag.MagneticIndex = 3;
GMAT LowEarthProp_ForceModel.SRP.Flux = 1367;
GMAT LowEarthProp_ForceModel.SRP.Nominal_Sun = 149597870.691;

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator LowEarthProp;
GMAT LowEarthProp.FM = LowEarthProp_ForceModel;
GMAT LowEarthProp.Type = RungeKutta89;
GMAT LowEarthProp.InitialStepSize = 60;
GMAT LowEarthProp.Accuracy = 9.999999999999999e-012;
GMAT LowEarthProp.MinStep = 0.001;
GMAT LowEarthProp.MaxStep = 2700;
GMAT LowEarthProp.MaxStepAttempts = 50;
GMAT LowEarthProp.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- Burns
%----------------------------------------

Create ImpulsiveBurn DefaultIB;
GMAT DefaultIB.CoordinateSystem = Local;
GMAT DefaultIB.Origin = Earth;
GMAT DefaultIB.Axes = VNB;
GMAT DefaultIB.Element1 = 0;
GMAT DefaultIB.Element2 = 0;
GMAT DefaultIB.Element3 = 0;
GMAT DefaultIB.DecrementMass = false;
GMAT DefaultIB.Isp = 300;
GMAT DefaultIB.GravitationalAccel = 9.810000000000001;

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create OrbitView DefaultOrbitView;
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ 0 0 ];
GMAT DefaultOrbitView.Size = [ 0 0 ];
GMAT DefaultOrbitView.RelativeZOrder = 0;
GMAT DefaultOrbitView.Add = {DigitalSat, Earth};
GMAT DefaultOrbitView.CoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.ViewPointReference = Earth;
GMAT DefaultOrbitView.ViewPointVector = [ -60000 30000 20000 ];
GMAT DefaultOrbitView.ViewDirection = Earth;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = EarthMJ2000Eq;
GMAT DefaultOrbitView.ViewUpAxis = Z;
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = Off;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = Off;
GMAT DefaultOrbitView.UseInitialView = On;
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = On;


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate LowEarthProp(DigitalSat) {DigitalSat.Earth.Periapsis};
