# Time-stamp: "2022-02-05 20:16:16 kastner"
# File: /home/kastner/smallsats/qascript/GuildApiStartupFile.py
#
#-------------------------------------------------------------------------------
# API Startup File Editor
#-------------------------------------------------------------------------------
# Replaces relative paths in the GMAT startup file with absolute paths for the
# API startup file, allowing the API to be run from any folder. This script is
# placed in the application/api directory of the GMAT installation folder. It
# generates two startup files:
# - api_startup_file.txt, generated from the startup file in the bin directory
# - api_startup_file_d.txt, generated from the startup file in the debug directory
#
#--------------------------------------------------------------
# Step 1: Assumes the GMAT base directory location
#--------------------------------------------------------------
# /home/vagrant/gmat/GMAT-R2020a-Linux-x64
#    (Note: The above is true for the pre-built OVA from SmallSats Group.
#           If your setup is different, then change the line in the
#           Python code below that sets "gmatdir = ...")
#
#--------------------------------------------------------------
# Step 2: Preparation
#--------------------------------------------------------------
# Edit /home/vagrant/gmat/GMAT-R2020a-Linux-x64/bin/gmat_startup_file.txt
#    (Note: Set your assumptions in gmat_startup-file.txt BEFORE
#           running this python code. See the detailed instructions
#           below.)
#
# Depending on how you start GMATd, your root will be
#    /home/vagrant/gmat
#    /home/vagrant/gmat/GMAT-R2020a-Linux-x64
# (the second one is probably the one your are using).
#
# So your gmat_startup_file.txt may be in different places:
#
#    Edit /home/vagrant/gmat/application/bin/gmat_startup_file.txt
# -or-
#    Edit /home/vagrant/gmat/GMAT-R2020a-Linux-x64/bin/gmat_startup_file.txt
#
#     Comment out MATLAB related lines (comment lines start with #):
#     #PLUGIN   = ../plugins/libMatlabInterface
#     #PLUGIN   = ../plugins/libFminconOptimizer
#     ...
#     #MATLAB_FUNCTION_PATH   = ROOT_PATH/matlab
#     #MATLAB_FUNCTION_PATH   = ROOT_PATH/userfunctions/matlab
#
# To avoid future confusion, you may also find that you need to
# explicitly change your root path
#
#     ROOT_PATH     = /home/vagrant/gmat/GMAT-R2020a-Linux-x64/
#
# Finally, you can comment out unused items that will cause errors messages
# during loading
#
#     # Internal-only-plugins
#     #PLUGIN   = ../plugins/proprietary/libCsaltInterfaced
#     #PLUGIN   = ../plugins/proprietary/libEMTGmodelsd
#     #PLUGIN   = ../plugins/proprietary/libMarsGRAMd
#     #PLUGIN   = ../plugins/proprietary/libMsise86d
#     #PLUGIN   = ../plugins/proprietary/libNRLMsise00d
#     #PLUGIN   = ../plugins/proprietary/libSNOptimizerd
#     #PLUGIN   = ../plugins/proprietary/libVF13Optimizerd
#
# Note: In the future, we may be able to substitute GNU's open source
#       replacement, Octave, for proprietary product MATLAB.
#
# Copy atmosphere drag force model into expected directory
#
#    cd /home/vagrant/gmat/application
#    cp samples/SupportFiles/CSSI_2004To2026.txt data/atmosphere/earth/
# -or-
#    cd /home/vagrant/gmat/GMAT-R2020a-Linux-x64
#    cp samples/SupportFiles/CSSI_2004To2026.txt data/atmosphere/earth/
#
#--------------------------------------------------------------
# Step 3: Will write two files into a GMATd directory
#--------------------------------------------------------------
# /home/vagrant/gmat/GMAT-R2020a-Linux-x64/api/api_startup_file.txt
# /home/vagrant/gmat/GMAT-R2020a-Linux-x64/api/api_startup_file_d.txt
#
#--------------------------------------------------------------
# Step 4: To use, start Python with this file as the argument
#--------------------------------------------------------------
# cd /home/vagrant/gmat/GMAT-R2020a-Linux-x64/api
# python3 ~/smallsats/qascripts/BuildApiStartupFile.py
#    (Note: The above assumes this file is in the given location.)
#
#--------------------------------------------------------------
# Step 5: Edit the api/load_gmatpy file
#--------------------------------------------------------------
# Edit the file: load_gmat.py
#    /home/vagrant/gmat/GMAT-R2020a-Linux-x64/api/load_gmat.py
#       Change the line "GmatInstall = "<TopLevelGMATFolder>"
#       to "GmatInstall = "/home/vagrant/gmat/GMAT-R2020a-Linux-x64"
#
#--------------------------------------------------------------
# Step 6: Run a GMAT script from Python
#--------------------------------------------------------------
# cd /home/vagrant/gmat/GMAT-R2020a-Linux-x64/
# python3 ~/smallsats/qascripts/Ex_R2020a_FindTheMoon.py
#
#
#===============================================================================

from os.path import dirname, abspath, basename, isdir

#gmatdir = dirname(dirname(abspath(__file__))) 	#GMAT directory
gmatdir = "/home/vagrant/gmat/GMAT-R2020a-Linux-x64"

#Generate the BIN startup file
binfilename = gmatdir + "/bin/gmat_startup_file.txt"
binfileout = open(gmatdir + "/bin/api_startup_file.txt", "w")

replacePattern = ".."
replaceVal = gmatdir

with open(binfilename) as binfile:
	line = binfile.readline()
	while line:
		outline = line.replace(replacePattern, replaceVal)
		binfileout.write(outline)

		line = binfile.readline()

binfileout.close()
print("Wrote {}".format(binfilename))

# If a debug build exists, generate the DEBUG startup file
if isdir(gmatdir + "/debug") :
	debugfilename = gmatdir + "/debug/gmat_startup_file.txt"
	debugfileout = open("api_startup_file_d.txt", "w")

	replacePattern = "./"
	replaceVal = gmatdir + "/"

	with open(debugfilename) as debugfile:
		line = debugfile.readline()
		while line:
			outline = line.replace(replacePattern, replaceVal)
			debugfileout.write(outline)

			line = debugfile.readline()

	debugfileout.close()
	print("Wrote {}".format(debugfilename))
else:
	print("Note: No debug build detected, so no debug startup generated.")
