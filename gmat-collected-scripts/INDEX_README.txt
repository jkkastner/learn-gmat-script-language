This file contains quick notes regarding the collection of GMAT Scripts
   - short description of the code
   - source of the GMAT script file(s)
   - author of the material, if known
   - copyright information, if known
   - relevant web page URLs, if known


Note:
   File extension standards for GMAT scripts:
     .script is a GMAT script
     .m is MATLAB or OCTAVE support code
     .py is Python support code


Script Examples from the GMAT source
Copyright US Goverment, NASA
   GMAT/source/GMAT/R2020a/docs/help/files/scripts/
      Tut_Mars_B_Plane_Targeting.script
      Tut_Mars_B_Plane_Targeting.script
      Ex_MinFuelLunarCapture.script
      Tut_MultipleShootingTutorial_Step3.script
      Tut_MultipleShootingTutorial_Step4.script
      Tut_HohmannTransfer.script
      Tut_Target_Finite_Burn_to_Raise_Apogee.script
      ReportTutorial.script
      LunarTransferTutorial.script
      Tut_MultipleShootingTutorial_Step2.script
      Tut_UsingGMATFunctions.script
      Ex_LowEarthMinFuelTransfer.script
      Ex_AlgebraicOptimization.script
      MarsBPlaneTutorial.script
      Tut_MultipleShootingTutorial_Step1.script
      ForceModelsTutorial.script
      LunarTransferToL2-tutorial.script
      LEOStationKeepingTutorial.script
      FormationRendezvousTutorial.script
      Tut_MultipleShootingTutorial_Step5.script
   GMAT/source/GMAT/R2020a/api/
      Ex_R2020a_ToLuna.script
   GMAT/source/GMAT/R2020a/matlab/libCInterface/
      GmatConfig.script
   GMAT/source/GMAT/R2020a/userincludes/
      CreateIncludeSatA.script
   GMAT/source/GMAT/R2020a/userincludes/
      InitializeNestedIncludeSatB.script
   GMAT/source/GMAT/R2020a/samples/
      Ex_R2020a_DynamicDisplay_QuaternionPropagation.script
      Ex_Attitude_NadirPointing.script
      Tut_Mars_B_Plane_Targeting.script
      Ex_LunarTransfer.script
      Ex_Attitude_Inertial.script
      Ex_R2015a_EclipseLocation.script
      NeedSNOPT/Ex_R2015a_OptFiniteBurn.script
      NeedSNOPT/Ex_R2015a_AlgebraicOptimization.script
      NeedSNOPT/Ex_R2015a_MinFuelLunarTransfer.script
      Ex_FiniteBurn.script
      Ex_TargetFiniteBurn_CenterPeriapsisTA.script
      Ex_HohmannTransfer.script
      Navigation/Simulate_Continuous_thrust_SolveFor_ScaleFactor.script
      Navigation/Ex_R2020a_Estimate_SPADDragScaleFactor.script
      Navigation/Ex_R2020a_Estimate_RangeSkin.script
      Navigation/Simulate_DSN_Range_then_Estimate_CartesianState_and_Cr.script
      Navigation/Ex_R2020a_Estimate_ThrustScaleFactor.script
      Navigation/Ex_R2017a_Simulate_and_Process_Range_and_RangeRate_data.script
      Navigation/Ex_R2018a_MergeEphemeris.script
      Navigation/Ex_R2018a_Keplerian_state_solve_for_with_apriori_covariance.script
      Navigation/Ex_EphemerisCompareGeneric.script
      Navigation/Ex_R2020a_Estimate_AngleData.script
      Navigation/Ex_Code500_EphemerisCompare.script
      Navigation/Ex_R2017a_Second_Level_Data_Editing.script
      Navigation/Ex_R2018a_CompareEphemeris.script
      Navigation/Ex_R2017a_Simulate_and_Process_GpsPosVec_data.script
      Navigation/Ex_R2020a_Estimate_SPADSRPScaleFactor.script
      Navigation/Ex_R2020a_Alpha_FilterSmoother.script
      Navigation/Ex_EphemerisMergeGeneric.script
      Tut_LEOStationKeeping.script
      Ex_R2016a_FiniteBurnParameters.script
      SupportFiles/Ex_IncludeFile.script
      NeedVF13ad/Tut_MultipleShootingTutorial_Step3.script
      NeedVF13ad/Ex_MinFuelLunarTransfer.script
      NeedVF13ad/Tut_MultipleShootingTutorial_Step4.script
      NeedVF13ad/Ex_MarsPatchConic.script
      NeedVF13ad/Tut_MultipleShootingTutorial_Step2.script
      NeedVF13ad/Ex_AlgebraicOptimization.script
      NeedVF13ad/Tut_MultipleShootingTutorial_Step1.script
      NeedVF13ad/Ex_OptimalLCROSSTrajectory.script
      NeedVF13ad/Ex_MarsLaunchWindowAnalysis.script
      NeedVF13ad/Ex_R2015a_LunarCubeSatWithElectricPropulsion.script
      NeedVF13ad/Tut_MultipleShootingTutorial_Step5.script
      NeedVF13ad/Ex_OptimalLunarApsidesRotation_2Burn.script
      Ex_R2015a_PythonInterface.script
      Ex_Attitude_ModifiedRodriquezParameters.script
      Ex_IncludeFile.script
      Tut_SimpleOrbitTransfer.script
      Ex_ForceModels.script
      Ex_R2016a_NewMathFunctions.script
      Ex_R2020a_Propagate_ThrustHistoryFile.script
      Ex_R2018a_CommandEcho_GEOTransfer.script
      Ex_MMSSingleLunarSwingby.script
      Ex_Basic3DModel.script
      Ex_R2016a_Code500Propagator.script
      Ex_SafetyEllipse.script
      Ex_R2015a_GMATFunction_UsingGlobalObjects.script
      Ex_MarsOrbit.script
      Ex_R2015a_SchattenFile_NominalCycle_Mean.script
      Ex_LCROSSTrajectory.script
      Ex_R2014a_HighFidelitySRP.script
      Ex_TargetFiniteBurn_CenterPeriapsisDuration.script
      Ex_R2015a_TargetStatusWindow.script
      Ex_Attitude_Spinner.script
      Ex_R2016a_NewFunctions_GetEphemStates.script
      Ex_R2014a_PrecessingSpinnerAttitude.script
      Tut_Target_Finite_Burn_to_Raise_Apogee.script
      Ex_R2016a_NewStringFunctions.script
      Ex_LunarOrbitStationKeeping.script
      Ex_R2018a_Yukon_MarsLaunchWindowAnalysis.script
      Ex_R2015a_ElectricPropulsion.script
      Ex_FileInterface.script
      Ex_R2015a_GMATFunction_Math.script
      Ex_R2014a_ImrovedTargeterAlgorithms.script
      Ex_R2016a_ThrusterWithMultipleTanks.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_DoubleLunarSwingby.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_LunarTransfer.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_RealTimeMOC.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_Yukon_MarsLaunchWindowAnalysis.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_SafetyEllipse.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_VR_2017SolarEclipse.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_MMSSingleLunarSwingby.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_Attitude_NadirPointing.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_ConstellationScript.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_HohmannTransfer.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_FieldOfView.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_LCROSSTrajectory.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_FiniteBurn.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_Attitude_Inertial.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_LibrationPointStationKeeping_ESM.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_SPICEOrbitAndAttitudePropagation.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_MarsOrbit.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_CustomSegmentColors.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_GEOTransfer.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_ElectricPropulsion.script
      NeedOpenFramesInterface/Ex_R2020a_OFI_GivenEpochGoToTheMoon.script
      OptimalControl/Ex_R2020a_EMTGSpacecraft_SCOpt_ThrustType1.script
      OptimalControl/Ex_R2020a_CelestialBodyRendezvous_Mars.script
      OptimalControl/Ex_R2020a_EarthToMarsSOI_C3Eq0_CSALTTutorial.script
      OptimalControl/Ex_R2020a_EMTGSpacecraft_SCOpt_BusPowerType0.script
      OptimalControl/Ex_R2020a_PCLaunch_ConstrainedC3AndDLA_EarthLaunch_EarthOrigin.script
      OptimalControl/Ex_R2020a_OCPropTest_GEO_4x4.script
      OptimalControl/Ex_R2020a_Phase_Type_ImplicitRKOrder6.script
      OptimalControl/Ex_R2020a_IntegratedFlyby_MarsFlyby.script
      OptimalControl/Ex_R2020a_EMTGSpacecraft_SCOpt_PowerSupplyCurve1.script
      Ex_ConstellationScript.script
      NeedMatlab/Ex_MatlabEnv.script
      NeedMatlab/Ex_CallMatlabFunctions.script
      Tut_UsingGMATFunctions.script
      Ex_MarsBPlane.script
      Ex_R2015a_CSSISpaceWeatherFile.script
      Ex_R2014a_MarsGRAM2005.script
      Ex_L2Design.script
      Ex_R2016a_EOPFileLocationFromScript.script
      Ex_GivenEpochGoToTheMoon.script
      Ex_R2016a_IncludeMacro.script
      Ex_R2014a_NewOrbitStateReps.script
      Ex_R2016a_WriteCommand.script
      Tut_ElectricPropulsionModelling.script
      Ex_R2014a_SolverConvergenceParameters.script
      Tut_Simulate_DSN_Range_and_Doppler_Data.script
      Ex_R2014a_CCSDSAEMAttitude.script
      Tut_Simulate_DSN_Range_and_Doppler_Data_3_weeks.script
      Ex_R2015a_StationContactLocator.script
      Ex_R2014a_NadirPointingAttitude.script
      Ex_LEOStationKeeping.script
      Ex_ControlFlow.script
      Tut_Orbit_Estimation_using_DSN_Range_and_Doppler_Data.script
      Ex_R2015a_GMATFunction_TargetInsideFunction.script
      Ex_Attitude_VNB.script
      Ex_MathInScript.script
      Ex_LibrationPointStationKeeping_ESM.script
      Ex_SPICEOrbitAndAttitudePropagation.script
      Ex_DoubleLunarSwingby.script
      Ex_GEOTransfer.script
      Ex_R2017a_STKEphemPropagation.script
      Ex_Integrators.script
      Tut_SimulatingAnOrbit.script
      Ex_R2018a_Yukon_AlgebraicOptimization.script
      Ex_R2014a_CustomSegmentColors.script
      Ex_R2016a_STKEphemOutput.script

Script Examples from the GMAT Documentation
Copyright US Goverment, NASA
   Ex_R2020a_BasicFM.m
   Ex_R2020a_BasicFM.py
   Ex_R2020a_BasicForceModel.m
   Ex_R2020a_BasicForceModel.py
   Ex_R2020a_CompleteForceModel.m
   Ex_R2020a_FindTheMoon.m
   Ex_R2020a_FindTheMoon.py
   Ex_R2020a_PropagationLoop.m
   Ex_R2020a_PropagationLoop.py
   Ex_R2020a_PropagationStep.m
   Ex_R2020a_PropagationStep.py
   Ex_R2020a_RangeMeasurement.m
   Ex_R2020a_RangeMeasurement.py
   Ex_R2020a_ToLuna.script

Alfono Continuous Low-Thrust Orbit Transfer Trajectory Simulations
   alfano-master by Colin Helms
      https://gitlab.com/digitalsats1/alfano/-/tree/master
   gmat-models-master by Colin Helms
      https://gitlab.com/digitalsats1/gmat-models/-/tree/master

Script Examples from GitHub
   GMAT-SaturnIcyMoonExplorer by David Mueller
      https://github.com/iamxanadu/GMAT-SaturnIcyMoonExplorer
   GMAT_scripts by Peter Nazarenko
      https://github.com/PeterNazarenko/GMAT_scripts
   Physics-IA-Simulation by Rohan-Goyal
      https://github.com/Rohan-Goyal/Physics-IA-Simulation
   ORIcubesat by Achim Vollhardi's LEO-to-GEO GMAT script
      (see; https://github-wiki-see.page/m/phase4space/p4xt/wiki/General-Mission-Analysis-Tool-%28GMAT%29-Scripts-and-Explanations)
      https://github.com/phase4space/p4xt/blob/main/GMAT/ORIcubesat.script
   GMAT_MArsClone_Mars_MARS50C_0_0.script by Tristan Moody
      https://github.com/SWGlassPit/GMAT-unicode/blob/master/R2011bFiles/GMAT_MarsClone_Mars_MARS50C_0_0.script


Script automation from GitHub
   GMAT-Automation by Colin Helms
      https://github.com/a093130/GMAT-Automation
   GMAT-automation by Matthew Griffth
      https://github.com/Matthew-Griffith/GMAT-automation

Random
   BoostingFromDecayingOrbit.script by John Kastner
   Ex_Attitude_Spinner.script from GMAT Documentation
   JKK_SimulatingADecayingOrbit.script by John Kastner
   New_SimulatingADecayingOrbit.script by John Kastner
   SimulatingADecayingOrbit.script by John Kastner

Toolbox for Matlab
   gMatlabToolbox by ger-e (not included in collection)
      https://github.com/ger-e/gMatlabToolbox

Miscellaneous from GitLab
   OpenFramesInterface by Ravi Mathur (not included in collection)
      https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface
